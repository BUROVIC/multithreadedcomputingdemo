// Lab1Threads.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <iomanip>
#include <omp.h>
#include <vector>
#include <thread>
#include <map>
#include <chrono>
#include <thread>
#include <algorithm>
#include <mutex>
#include "ThreadPool.h"
using namespace std;

typedef void(*GaussSolverFunction) (double** a, int n, double* x);

void solve_gauss(double** a, int n, double* x)
{
    for (auto k = 1; k < n; k++) // ������ ���
    {
        for (auto j = k; j < n; j++)
        {
            const auto m = a[j][k - 1] / a[k - 1][k - 1];
            for (auto i = 0; i < n + 1; i++)
            {
                a[j][i] = a[j][i] - m * a[k - 1][i];
            }
        }
    }

    for (auto i = n - 1; i >= 0; i--)
    {
        x[i] = a[i][n] / a[i][i];

        for (auto c = n - 1; c > i; c--)
        {
            x[i] = x[i] - a[i][c] * x[c] / a[i][i];
        }
    }
}

void solve_gauss_multi_thread(double** a, int n, double* x)
{
    int k, j, i, c;
    double m;

    auto nthreads = 4;

    ThreadPool pool(nthreads);
    std::vector<std::future<void>> results(nthreads);

    for (k = 1; k < n; k++) // ������ ���
    {
        for (int t = 0; t < nthreads; t++)
        {
            results[t] = pool.enqueue([](const int start, const int step, int k, double** a, int n)
                {
                    for (auto j = k + start; j < n; j += step)
                    {
                        const auto m = a[j][k - 1] / a[k - 1][k - 1];
                        for (auto i = 0; i < n + 1; i++)
                        {
                            a[j][i] = a[j][i] - m * a[k - 1][i];
                        }
                    }

                }, t, nthreads, k, a, n);
        }

        for (auto& thread : results)
        {
            thread.get();
        }

    }


    for (i = n - 1; i >= 0; i--) // �������� ���
    {
        x[i] = a[i][n] / a[i][i];

        for (c = n - 1; c > i; c--)
        {
            x[i] = x[i] - a[i][c] * x[c] / a[i][i];
        }
    }
}

void solve_gauss_multi_thread_racing(double** a, int n, double* x)
{
    int k, j, i, c;
    double m;

    auto nthreads = 4;
    std::vector<std::thread> threads(nthreads);
    for (int t = 0; t < nthreads; t++)
    {
        threads[t] = std::thread([](const int start, const int step, double** a, int n)
            {
                for (auto k = start + 1; k < n; k += step) // ������ ���
                {
                    for (auto j = k; j < n; j++)
                    {
                        const auto m = a[j][k - 1] / a[k - 1][k - 1];
                        for (auto i = 0; i < n + 1; i++)
                        {
                            a[j][i] = a[j][i] - m * a[k - 1][i];
                        }
                    }
                }
            }, t, nthreads, a, n);
    }

    for (auto& thread : threads)
    {
        thread.join();
    }

    for (i = n - 1; i >= 0; i--) // �������� ���
    {
        x[i] = a[i][n] / a[i][i];

        for (c = n - 1; c > i; c--)
        {
            x[i] = x[i] - a[i][c] * x[c] / a[i][i];
        }
    }
}

void solve_gauss_openmp(double** a, int n, double* x)
{
    omp_set_num_threads(4); // ����� �������

    for (auto k = 1; k < n; k++) // ������ ���
    {
#pragma omp parallel for
        for (auto j = k; j < n; j++)
        {
            const auto m = a[j][k - 1] / a[k - 1][k - 1];
            for (auto i = 0; i < n + 1; i++)
            {
                a[j][i] = a[j][i] - m * a[k - 1][i];
            }
        }
    }

    for (auto i = n - 1; i >= 0; i--) // �������� ���
    {
        x[i] = a[i][n] / a[i][i];

        for (auto c = n - 1; c > i; c--)
        {
            x[i] = x[i] - a[i][c] * x[c] / a[i][i];
        }
    }
}

double** generate_matrix(int n)
{
    auto **a = new double *[n];

    for (auto i = 0; i < n; i++)
        a[i] = new double[n + 1];

    for (auto i = 0; i < n; i++)
    {
        for (auto j = 0; j < n + 1; j++)
        {
            a[i][j] = std::rand() % 1000000;
        }
    }

    return a;
}

double** get_matrix_copy(double** a, int n)
{
    auto **a1 = new double *[n];

    for (auto i = 0; i < n; i++)
    {
        a1[i] = new double[n + 1];
        for (auto j = 0; j < n + 1; j++)
        {
            a1[i][j] = a[i][j];
        }
    }

    return a1;
}

void print_matrix(double ** a, int n)
{
    for (auto i = 0; i < n; i++)
    {
        for (auto j = 0; j < n + 1; j++)
        {
            cout << setw(10) << "a[ " << i << "," << j << "]= " << a[i][j];
        }
        cout << endl;
    }
}

void print_matrix(double * x, int n)
{
    for (auto i = 0; i < n; i++)
        cout << "x[" << i << "]=" << x[i] << " " << endl;
}

int main()
{
    map<GaussSolverFunction, string> my_map = {
        { solve_gauss, "Default gauss solver" },
        { solve_gauss_multi_thread, "Multi thread gauss solver" },
        { solve_gauss_openmp, "Openmp gauss solver" }
    };

    const auto print_matrices = false;
    const auto n_step = 100;
    const auto n_max = 2000;
    const auto number_of_attempts = 5;

    //const auto print_matrices = true;
    //const auto n_step = 3;
    //const auto n_max = 3;
    //const auto number_of_attempts = 1;

    for (auto n = n_step; n <= n_max; n += n_step)
    {
        cout << "N = " << n << endl;
        const auto a = generate_matrix(n);

        if (print_matrices)
        {
            print_matrix(a, n);
        }

        double* correct_answer = nullptr;
        for (auto value : my_map)
        {
            cout << value.second + ":" << endl;
            double total_time = 0;

            auto is_answer_incorrect = false;
            for (auto attempt = 0; attempt < number_of_attempts; attempt++)
            {
                const auto a1 = get_matrix_copy(a, n);

                auto *x1 = new double[n];
                for (auto variable_number = 0; variable_number < n; variable_number++)
                {
                    x1[variable_number] = 1;
                }

                const auto start_time = omp_get_wtime();
                value.first(a1, n, x1);
                total_time += omp_get_wtime() - start_time;

                if (print_matrices)
                {
                    print_matrix(x1, n);
                }

                if (correct_answer == nullptr && value.first == solve_gauss)
                {
                    correct_answer = x1;
                }
                else if (!is_answer_incorrect)
                {
                    for (auto variable_number = 0; variable_number < n; variable_number++)
                    {
                        if (x1[variable_number] != correct_answer[variable_number])
                        {
                            is_answer_incorrect = true;
                            break;
                        }
                    }
                    delete[] x1;
                }

                delete a1;
            }

            if (is_answer_incorrect)
            {
                cout << "INCORRECT ANSWER" << endl;
            }

            const auto average_time = total_time / number_of_attempts;

            printf("Work took %f milliseconds\n", average_time * 1000);

            cout << endl;
        }

        delete[] correct_answer;

        cout << "_________________________________________" << endl << endl;
        delete a;
    }

    system("pause");
    return 0;
}
